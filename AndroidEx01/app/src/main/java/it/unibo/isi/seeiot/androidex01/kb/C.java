package it.unibo.isi.seeiot.androidex01.kb;

public class C {

    public static final String LOG_TAG = "AndroidEx01";

    public class login {
        public static final String USERNAME = "mario.rossi";
        public static final String PASSWORD = "test1234";
    }

    public class dweet {
        public static final String FOR_PREFIX = "https://dweet.io/dweet/for/";

        public static final String COLLECTION_RESOURCE_NAME = "seeiot2018_androidex";

        public static final int NEW_LOCATION_DETECTED_MESSAGE = 1;
        public static final String NEW_LOCATION_DETECTED_MESSAGE_LATITUDE_KEY = "latitude";
        public static final String NEW_LOCATION_DETECTED_MESSAGE_LONGITUDE_KEY = "longitude";
    }
}
